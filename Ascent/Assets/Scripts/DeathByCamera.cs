﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement; 

public class DeathByCamera : MonoBehaviour {

	private float invisibleTime = 0f; 
	private bool invisible = false; 
	public float deathTime; 

	// Update is called once per frame
	void Update () {
		if (invisible)
			invisibleTime += Time.deltaTime; 
		if (invisibleTime > deathTime) { 
			if (CompareTag ("Player"))
				SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);  
			else 
				Destroy (gameObject); 
		}
	}

	void OnBecameInvisible() {
		invisible = true; 
	}

	void OnBecameVisible() {
		invisible = false; 
		invisibleTime = 0; 
	}
}
