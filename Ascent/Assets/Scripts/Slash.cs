﻿using UnityEngine;
using System.Collections;

public class Slash : MonoBehaviour {
	
	public float duration; 
	public GameObject owner; 

	private float lifetime;

	void Start () { 
		lifetime = 0f; 
	}

	void Update () {
		lifetime += Time.deltaTime; 
		if (lifetime > duration)
			Destroy (gameObject); 
	}
}
