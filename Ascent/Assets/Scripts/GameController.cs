﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	public GameObject background; 
	public GameObject platform; 
	public Transform camera; 

	private float nextSpawn = 0f; 
	private float cameraTop; 
	void Start() { 
		cameraTop = camera.position.y + 5; 
		//spawns the starting platforms 
		for(int i = -5; i < cameraTop; i += 2)
			Instantiate (platform, new Vector3 (Random.Range (-6, 6), Random.Range (i, i + 1.5f), 1), Quaternion.identity);
	}
	// Update is called once per frame
	void Update () {
		cameraTop = camera.position.y + 5;  
		if (Time.time > nextSpawn) {
			for (int i = 0; i < 2; i++) {
				Instantiate (platform, new Vector3 (Random.Range (-6, 6), Random.Range (cameraTop, cameraTop + 1.5f), 1), Quaternion.identity);
			}
			nextSpawn += 2f; 
		}
	}
}
