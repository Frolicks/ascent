﻿using UnityEngine;
using System.Collections;

public class PlatformSpawner : MonoBehaviour {
	public GameObject background; 
	public GameObject platform; 
	public GameObject startingPlatform; 

	public Transform camera; 

	public float platDist; 

	private float nextSpawn; 
	private float cameraTop, maxStartPlatY; 

	void Start() {  
		cameraTop = 9; 

		//spawns the starting platforms 
		for (maxStartPlatY = startingPlatform.transform.position.y + platDist; maxStartPlatY < cameraTop; maxStartPlatY += platDist) {
			GameObject spawnedPlatform = (GameObject)Instantiate (platform, new Vector3 (Random.Range (-6, 6), maxStartPlatY, 1), Quaternion.identity);
			spawnedPlatform.transform.localScale = new Vector3 (Random.Range (0.1f, 0.4f), 0.02f, 1f);  
		}
		cameraTop = maxStartPlatY - platDist; //crude adjustment to transition from startspawn platforms
		nextSpawn = maxStartPlatY - platDist;
	}

	// Update is called once per frame
	void Update () {
		if (cameraTop >= nextSpawn) {
			for (int i = 0; i < 1; i++) {
				GameObject spawnedPlatform = (GameObject) Instantiate (platform, new Vector3 (Random.Range (-6, 6), cameraTop + platDist, 1), Quaternion.identity);
				spawnedPlatform.transform.localScale = new Vector3 (Random.Range (0.1f, 0.4f), 0.02f, 1f);  
			}
			nextSpawn += platDist * Random.Range(0.9f, 1.5f); 
		}

		cameraTop = camera.position.y + 5;
	
	}
}
