﻿using UnityEngine;
using System.Collections;

public class PlayerCollisions : MonoBehaviour {

    public int numMarsh;
    [HideInInspector]
    public GameObject ownSlash; 

	void Awake() {
        //numMarsh = GetComponent<PlayerController>().numMarsh;
	}
    void Update()
    {
    }
	void OnCollisionEnter2D(Collision2D other) { 
		if (other.gameObject.CompareTag ("bullet")) { 
			transform.position = Vector3.zero; 
		} 
		if(other.gameObject.CompareTag("marsh")) {
			GetComponent<PlayerController>().numMarsh++;
			Destroy (other.gameObject); 
		}
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("slash"))
        {
            if (!other.gameObject.GetInstanceID().Equals(ownSlash.GetInstanceID()))
                gameObject.transform.position = Vector3.zero;
        }
    }
		
}
