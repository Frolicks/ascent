﻿using UnityEngine;
using System.Collections;

public class MarshSpawner : MonoBehaviour {
	public float openingLength, shootRate, velX;
	public GameObject marsh; 

	private float nextSpawn; 
	// Update is called once per frame
	void Awake() { 
		nextSpawn = Time.time;
	}

	void Update () {
		if(Time.time > nextSpawn) { 	
			GameObject spawnedMarsh =  (GameObject) Instantiate (marsh, new Vector2 (Random.Range (-openingLength / 2, openingLength / 2), transform.position.y), Quaternion.identity); 
			spawnedMarsh.transform.localScale = Vector3.one * Random.Range (0.3f, 1f); 
			spawnedMarsh.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range (-velX, velX), transform.position.y + Random.Range(400,800)));
			nextSpawn += shootRate * Random.Range(1f, 3f); 
		}
	}
}
