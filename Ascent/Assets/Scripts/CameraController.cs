﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public float cameraSpeed; 

	// Update is called once per frame
	void Update() {
		Invoke ("panUp", 5f); 
	}

	public void panUp() { 
		transform.position = new Vector3 (0f, transform.position.y + cameraSpeed * Time.deltaTime, -1f);
	}
}
