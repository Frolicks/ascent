﻿using UnityEngine;
using System.Collections;

public class BulletBehavior : MonoBehaviour {
	// Update is called once per frame
	public float velocity; 

	private Rigidbody2D rb; 

	void Start () { 
		rb = GetComponent<Rigidbody2D> ();  
		rb.velocity = transform.up * velocity; 
	}

	void Update () {
		rb.velocity = transform.up * velocity; 
	}

	void OnCollisionEnter2D(Collision2D other) { 
		if(other.gameObject.CompareTag("bullet")){ 
			this.transform.localScale = Vector3.one * 1.2f; 
			this.rb.gravityScale = 5f;
			rb.velocity = Vector2.zero;
			Debug.Log ("IM KILLING MYSELF");
			Invoke("killMySelf", 1f);

		}
	}

	void OnTriggerEnter2D(Collider2D other) { 
		if (other.gameObject.CompareTag ("slash")) {
			velocity = -1 * velocity; 
			//rb.velocity = transform.up * -velocity; 
			Debug.Log ("test");
		}
	}

	private void killMySelf() {
		Destroy (gameObject); 
	}
}