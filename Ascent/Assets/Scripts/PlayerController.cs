﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	[HideInInspector] public bool facingRight = true;

	public GameObject bullet, slashObj;

    public int numMarsh; 
	public float moveForce = 365f; 
    public float maxSpeed = 5f;
    public float jumpForce = 20f;
	public float shootCD, slashCD;
	public float canShootSize, shootRecoil; 
    public Transform groundCheck;

	public KeyCode left, right, up, down, jump, shoot, slash; 

    private bool grounded = false;
	private float nextShot, nextSlash;
	private PlayerCollisions pCol;
    
    private Rigidbody2D rb2d;

        // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
		pCol = GetComponent<PlayerCollisions> ();
        numMarsh = 0;  
    }

    void Update()
    {
        Debug.Log(numMarsh); 
        if (Input.GetKeyDown(jump) && grounded)
        {
            //anim.SetTrigger("Jump");
            rb2d.AddForce(Vector2.up * jumpForce);
        }
    }
		
    void FixedUpdate()
	{
		if (Input.GetKey (right)) rb2d.AddForce (Vector2.right * moveForce); 
		if (Input.GetKey (left)) rb2d.AddForce (Vector2.left * moveForce); 

		rb2d.velocity = new Vector2(Mathf.Clamp (rb2d.velocity.x, -1 * maxSpeed, maxSpeed), rb2d.velocity.y); 

		if (Input.GetKey(right) && !facingRight) Flip ();
		else if (Input.GetKey(left) && facingRight) Flip ();

		//BULLET FIRING
		if(Input.GetKeyDown(shoot) && Time.time > nextShot && !Input.GetKeyDown(slash) && numMarsh > 0) {
			if (Input.GetKey (up)) {
				Instantiate (bullet, new Vector3 (transform.position.x, transform.position.y + 0.8f, 0f), Quaternion.Euler (0f, 0f, 0f));
				rb2d.AddForce (Vector2.down * shootRecoil); 
			}
			else if (Input.GetKey (down)) {
				Instantiate (bullet, new Vector3 (transform.position.x, transform.position.y - 0.8f, 0f), Quaternion.Euler(0f, 0f, 180f)); 
				rb2d.AddForce(Vector2.up * shootRecoil); 
			}
			else if (facingRight) {
				Instantiate (bullet, new Vector3 (transform.position.x + 0.5f , transform.position.y, 0f), Quaternion.Euler(0f, 0f, -90f)); 
				rb2d.AddForce(Vector2.left * shootRecoil); 
			} 
			else {
				Instantiate (bullet, new Vector3 (transform.position.x - 0.5f, transform.position.y, 0f), Quaternion.Euler(0f, 0f, 90f)); 
				rb2d.AddForce(Vector2.right *shootRecoil);
			}
			numMarsh--;
			nextShot = Time.time + shootCD; 
		}

		//SLASHING 
		if(Input.GetKeyDown(slash) && Time.time > nextSlash && numMarsh > 0) {
			GameObject spawnedSlash;
			if (Input.GetKey (up)) {
				spawnedSlash = (GameObject)Instantiate (slashObj, new Vector3 (transform.position.x, transform.position.y + 0.8f, 0f), Quaternion.Euler (0f, 0f, 0f));
			}
			else if (Input.GetKey (down)) {
				spawnedSlash = (GameObject)Instantiate (slashObj, new Vector3 (transform.position.x, transform.position.y - 0.8f, 0f), Quaternion.Euler(0f, 0f, 180f)); 
			}
			else if (facingRight) {
				spawnedSlash = (GameObject)Instantiate (slashObj, new Vector3 (transform.position.x + 0.5f , transform.position.y, 0f), Quaternion.Euler(0f, 0f, -90f)); 
			} 
			else {
				spawnedSlash = (GameObject)Instantiate (slashObj, new Vector3 (transform.position.x - 0.5f, transform.position.y, 0f), Quaternion.Euler(0f, 0f, 90f)); 
			}
			numMarsh--;
            pCol.ownSlash = spawnedSlash; 
			nextSlash = Time.time + slashCD; 
		}

		//temporary loaded-ready visuals
		if (Time.time > nextShot && Time.time > nextSlash)
			this.transform.localScale = new Vector2 (canShootSize * Mathf.Sign(transform.localScale.x), canShootSize);
		else
			this.transform.localScale = new Vector2 (0.35f * Mathf.Sign(transform.localScale.x) , 0.33f); 
		
		grounded = Physics2D.Linecast (transform.position, groundCheck.transform.position);
		
	}


    void Flip()
    {
	    facingRight = !facingRight;
	    Vector3 theScale = transform.localScale;
	    theScale.x *= -1;
	    transform.localScale = theScale;
    }

}